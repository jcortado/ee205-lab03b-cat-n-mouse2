#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}


THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))


while (( catGuess !=  THE_NUMBER_IM_THINKING_OF ))
do
  
   read -p "Ok cat, I'm thinking of a number from one to $THE_MAX_VALUE. Make a guess: " catGuess;

   if (( catGuess < 1 ))
   then
      echo "You must enter a number >= 1"
   elif (( catGuess > THE_MAX_VALUE ))
   then
      echo "You must enter a number <= $THE_MAX_VALUE"
   elif (( catGuess > THE_NUMBER_IM_THINKING_OF ))
   then
      echo "No cat... the number I'm thinking of is lower than $catGuess"
   elif (( catGuess < THE_NUMBER_IM_THINKING_OF))
   then
      echo "No cat... the number I'm thinking of is higher than $catGuess"
   fi
done

echo "You got me. "
echo " \\    /\\ "
echo "  )  ( ') "
echo " (  /  ) "
echo "  \(__)| "
